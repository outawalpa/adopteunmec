<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class AdminUserType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', null, [
                'label' => 'Nom d\'utilisateur',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Vous devez saisir un nom d\'utilisateur'
                    ])
                ]
            ])
            
            ->add('email', EmailType::class, [
                'label' => 'Email',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Vous devez saisir une adresse email'
                    ]),
                    new Email([
                        'message' => 'L\'adresse email n\'est pas valide'
                    ])
                ]
            ])

            ->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $event) {
                $user = $event->getData();
                $form = $event->getForm();

                if (!$user || $user->getId() === null) {
                    $label = 'Ajouter';
                    $form->add('plainPassword', PasswordType::class, [
                        'label' => 'Mot de passe',
                        'constraints' => [
                            new NotBlank([
                                'message' => 'Vous devez saisir un mot de passe'
                            ]),
                        ]
                    ]);
                } else {
                    $label = 'Modifier';
                }


                $form
                    ->add('profil', null, [
                        'label' => 'Profil',
                        'placeholder' => 'Aucun',
                    ])

                    ->add('roles', ChoiceType::class, [
                        'multiple' => true,
                        'expanded' => true,
                        'choices'  => [
                            'Utilisateur'    => 'ROLE_USER',
                            'Administrateur' => 'ROLE_ADMIN'
                        ],
                        'constraints' => [
                            new NotBlank([
                                'message' => 'Vous devez sélectionner au moins un rôle',
                            ])
                        ]
                    ])
                
                    ->add('submit', SubmitType::class, [
                        'label' => $label
                    ])
                ;
            })
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_user';
    }
}